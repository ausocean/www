/*
	LICENSE
	Copyright (C) 2023 the Australian Ocean Lab (AusOcean)

	This file is part of the AusOcean website. The AusOcean website
	is free software: you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published
	by the Free Software Foundation, either version 3 of the License,
	or (at your option) any later version.

	The AusOcean website is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with NetReceiver in gpl.txt.  If not, see
	<http://www.gnu.org/licenses/>.
*/

package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/ausocean/www/gsheets"
	"golang.org/x/exp/slices"
)

type PageData struct {
	Header    template.HTML
	Footer    template.HTML
	Copyright template.HTML
}

var pageData PageData

type Product struct {
	Id         string
	Price      string
	Info       string
	Highlights []string
	Link       string
	ComingSoon bool
}

var simplePages = []string{
	"/",
	"/about",
	"/about/news",
	"/blog",
	"/jobs",
	"/license",
	"/license/content2019",
	"/literacy",
	"/networkblue",
	"/store",
	"/support",
	"/support/partners",
	"/support/supporters",
	"/technology",
}

func main() {
	// Create a new mux.
	mux := http.NewServeMux()

	// Parse header and footer files.
	parseHeaderFooter()

	// Redirect file requests to static dir.
	mux.Handle("/s/", http.StripPrefix("/s", http.FileServer(http.Dir("s"))))

	// Assign function handlers to each page.
	mux.HandleFunc("/node_modules/", nodeHandler)
	mux.HandleFunc("/store", storeHandler)
	mux.HandleFunc("/", simplePageHandler)
	mux.HandleFunc("/favicon.ico", faviconHandler)

	// Create a HTTP server.
	log.Println("listening on port: 8080")
	log.Fatal(http.ListenAndServe(":8080", mux))

}

// simplePageHandler handles requests to pages which have no additional
// backend functionality over inserting the header and footer to the html.
// All the pages that are supported by this handler are stored in the t/ directory
// where the file (*.html) matches the path of the request (including any folder
// structures). The home page is mapped to t/index.html.
//
// The pages that should be included in this handler should be added to the global
// simplePages variable.
//
// Pages that are not found will be redirected to the index page
func simplePageHandler(w http.ResponseWriter, r *http.Request) {
	// Log the request.
	log.Println(r.URL.Path)

	var pageName string
	// Check that the page is valid.
	if r.URL.Path == "/" {
		pageName = "/index"
	} else if pageName = strings.TrimSuffix(r.URL.Path, "/"); !slices.Contains(simplePages, pageName) {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// Parse the file.
	tmpl, err := template.ParseFiles("t" + pageName + ".html")
	if err != nil {
		log.Println("could not parse file:", pageName, ":", err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = tmpl.Execute(w, pageData)
	if err != nil {
		http.Error(w, "failed to write template", 500)
	}
}

// storeHandler handles requests to the store page. It utilises the Google
// Sheets API to update the contents of the store based on the Store Contents Sheet.
func storeHandler(w http.ResponseWriter, r *http.Request) {
	// Log the request.
	log.Println(r.URL.Path)

	// Authenticate google sheets API.
	creds, err := gsheets.AuthGSheetsRead(r.Context())
	if err != nil {
		log.Println("Couldn't get credentials:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Read from the "Store Contents" sheet at "Social media/Website".
	values, err := gsheets.ReadSpreadsheet(r.Context(), creds, "1kUXSb5-d4heDt_g8uzlV-cmh8K7OmXa8HK1r9iFsK3k", "Active")
	if err != nil {
		log.Println("couldn't read spreadsheet:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Parse the file.
	tmpl, err := template.ParseFiles("t/store.html")
	if err != nil {
		log.Println("could not parse t/store.html:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var products []Product
	for _, row := range values[1:] {
		// Parse the highlights.
		rawhighlights := row[3].(string)
		highlights := strings.Split(rawhighlights, ",")
		products = append(products, Product{
			Id:         row[0].(string),
			Price:      row[1].(string),
			Info:       row[2].(string),
			Highlights: highlights,
			Link:       row[4].(string),
		})

		if row[5] == "TRUE" {
			products[len(products)-1].ComingSoon = true
		}
	}
	storeData := struct {
		Products []Product
		HTMLData PageData
	}{Products: products, HTMLData: pageData}

	err = tmpl.Execute(w, storeData)
	if err != nil {
		log.Println("failed to write template:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}

// faviconHandler returns the favicon.ico file when the browser requests it.
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "favicon.ico")
}

// parseHeaderFooter parses the header and footer html files in t/, and saves
// them in the global pageData struct to be passed into template executions.
func parseHeaderFooter() {
	footer, err := os.ReadFile("t/footer.html")
	if err != nil {
		log.Fatalf("could not load footer")
	}
	pageData.Footer = template.HTML(string(footer))

	header, err := os.ReadFile("t/header.html")
	if err != nil {
		log.Fatalf("could not load header")
	}
	pageData.Header = template.HTML(string(header))

	copyright, err := os.ReadFile("t/copyright.html")
	if err != nil {
		log.Fatalf("could not load copyright")
	}
	pageData.Copyright = template.HTML(string(copyright))
}

// nodeHandler redirects requests to /node_modules to their relevant unpkg.com modules.
func nodeHandler(w http.ResponseWriter, r *http.Request) {
	module := strings.TrimPrefix(r.URL.Path, "/node_modules/")

	log.Println("Handling Node request to:", r.URL.Path)

	switch module {
	case "lit":
		http.Redirect(w, r, "https://unpkg.com/lit-element/lit-element.js?module", http.StatusPermanentRedirect)
		return
	case "lit/decorators.js":
		http.Redirect(w, r, "https://unpkg.com/lit/decorators.js?module", http.StatusPermanentRedirect)
		return
	default:
		w.WriteHeader(http.StatusNotFound)
		return
	}
}
