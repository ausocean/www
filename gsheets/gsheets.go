package gsheets

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

// AuthGSheetsRead is a wrapper for the google oath2 credentials from JSON method. It returns
// a readonly google credential.
func AuthGSheetsRead(ctx context.Context) (*google.Credentials, error) {
	return google.FindDefaultCredentials(ctx, "https://www.googleapis.com/auth/spreadsheets.readonly")
}

// ReadSpreadsheet returns the range of values specified in A1 or R1C1 notation (developer.google.com/sheets/api/guides/concepts#cell),
// of the given spreadsheet.
func ReadSpreadsheet(ctx context.Context, credentials *google.Credentials, spreadsheetID, readRange string) ([][]interface{}, error) {
	// Create a new service.
	srv, err := sheets.NewService(ctx, option.WithCredentials(credentials))
	if err != nil {
		return nil, fmt.Errorf("couldn't create new service: %w", err)
	}

	resp, err := srv.Spreadsheets.Values.Get(spreadsheetID, readRange).Do()
	if err != nil {
		return nil, fmt.Errorf("unable to read from sheet: %w", err)
	}

	if len(resp.Values) == 0 {
		return nil, errors.New("no data found")
	} else {
		return resp.Values, nil
	}
}
