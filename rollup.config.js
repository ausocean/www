import typescript from 'rollup-plugin-typescript2';
import resolve from '@rollup/plugin-node-resolve';

export default [
  {
    input: 'ts/store-elements.ts',
    output: {
      file: 's/store-elements.js',
      format: 'iife',
      name: 'storeElement',
      globals: {
        lit: 'lit',
        'lit/decorators.js': 'decorators_js'
      }
    },
    plugins: [
      resolve(),
      typescript()
    ]
  },
];