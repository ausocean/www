import { LitElement, html, css } from '../node_modules/lit';
import { customElement, property } from '../node_modules/lit/decorators.js';

@customElement('ausocean-product')
export class AusOceanProduct extends LitElement {
	// Style the element.
	static styles = [css`
		.product {
			padding: 25px 0px 25px 25px;
			display: flex;
			background-color: #dae1eb;
			border-radius: 25px;
			font-family: inherit;
			margin-bottom: 25px;
		}
		
		.prod-info {
			width: 100%;
			padding: 0px 25px;
			color: #2a2b2e;
			font-size: 100%;
			text-align: justify;
			display: flex;
			flex-direction: column;
		}

		button {
			margin-top: 25px;
			font-family: Ubuntu;
		}
		
		.prod-disp {
			width: 100%;
			overflow: hidden;
			background-color: white;
			border-radius: 25px;
			filter: drop-shadow(5px 5px 5px #1f3555);
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			color: #2a2b2e;
			font-family: Ubuntu;
			font-size: larger;
			text-align: center;
		}

		.prod-disp h3 {
			background-color: #ffffffab;
			padding: 25px;
			border-radius: 25px;
			z-index: 1000;
		}
		
		@media (max-width: 600px) {
			.product {
				flex-direction: column;
				padding: 25px;
			}
		
			.prod-info {
				width: 100%;
				padding: 25px 0px 0px 0px;
			}

			.prod-disp {
				width: 100%;
			}
		}
		
		img {
			width: 100%;
			transition: 0.1s all ease-in-out;
		}

		img:hover {
			scale: 1.2;
		}
		
		button {
			border-radius: 25px;
			padding: 10px 25px;
			font-size: 25px;
			align-self: flex-end;
			background-color: white;
			border: 0px;
			filter: drop-shadow(5px 5px 5px #1f3555);
		}
		
		p {
			margin: 0px;
		}

	::slotted(.popout) {
		margin: 10px;
		align-self: center;
		padding: 10px 20px 10px 35px;
		border-radius: 25px;
		background-color: #2f4f7f;
		color: white;
		text-align: left;
		font-size: inherit;
		width: fit-content;
	}

.coming-soon{
	color: #fbaf17ff;
}
	`];

	// Path to product image.
	@property()
	src: string;

	// Name of product (displayed under image).
	@property()
	name: string;

	// Price of product.
	@property()
	price: string;

	// Boolean to determine to add coming soon label.
	@property({ type: Boolean })
	soon: boolean;

	// Render the element.
	render() {
		return html`
		<div class="product">
			<div class="prod-disp">${this.soon ?
				html`<h3 class="coming-soon">Coming Soon</h3>` : html``}
				<img src="${this.src}">
				<h3>${this.name}</h3>
			</div>
			<div class="prod-info">
				<slot></slot>
				<button>$${this.price}</button>
			</div>
			
		</div>
		`;
	}
}
