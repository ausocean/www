(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-92175665-1', 'auto');
ga('send', 'pageview');
function countDays(from, id) {
  var elapsed = Date.now() - (new Date(from)).getTime();
  var days = Math.floor(elapsed / (60 * 60 * 24 * 1000));
  var elem = document.getElementById(id);
  elem.getElementsByClassName('days')[0].innerHTML = days;
}
function init() {
}
