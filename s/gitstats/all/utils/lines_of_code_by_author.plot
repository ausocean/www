set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'lines_of_code_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Lines"
set xtics rotate
set bmargin 6
plot 'lines_of_code_by_author.dat' using 1:2 title "Alex Arends" w lines, 'lines_of_code_by_author.dat' using 1:3 title "Alan Noble" w lines, 'lines_of_code_by_author.dat' using 1:4 title "Saxon Nelson-Milton" w lines, 'lines_of_code_by_author.dat' using 1:5 title "Saxon Milton" w lines, 'lines_of_code_by_author.dat' using 1:6 title "richardsonjack" w lines, 'lines_of_code_by_author.dat' using 1:7 title "Frank Cui" w lines, 'lines_of_code_by_author.dat' using 1:8 title "Saxon" w lines, 'lines_of_code_by_author.dat' using 1:9 title "Harry Telford" w lines, 'lines_of_code_by_author.dat' using 1:10 title "Dan Kortschak" w lines, 'lines_of_code_by_author.dat' using 1:11 title "scruzin" w lines, 'lines_of_code_by_author.dat' using 1:12 title "Trek H" w lines, 'lines_of_code_by_author.dat' using 1:13 title "Alex" w lines, 'lines_of_code_by_author.dat' using 1:14 title "kortschak" w lines, 'lines_of_code_by_author.dat' using 1:15 title "Joel Stanley" w lines, 'lines_of_code_by_author.dat' using 1:16 title "Scott" w lines, 'lines_of_code_by_author.dat' using 1:17 title "Unknown" w lines, 'lines_of_code_by_author.dat' using 1:18 title "Raul Vera" w lines, 'lines_of_code_by_author.dat' using 1:19 title "saxon" w lines, 'lines_of_code_by_author.dat' using 1:20 title "Jack Richardson" w lines, 'lines_of_code_by_author.dat' using 1:21 title "Trek Hopton" w lines
