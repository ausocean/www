set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "scruzin" w lines, 'commits_by_author.dat' using 1:3 title "Alan Noble" w lines, 'commits_by_author.dat' using 1:4 title "Saxon Nelson-Milton" w lines, 'commits_by_author.dat' using 1:5 title "Trek H" w lines, 'commits_by_author.dat' using 1:6 title "Saxon Milton" w lines, 'commits_by_author.dat' using 1:7 title "Trek Hopton" w lines, 'commits_by_author.dat' using 1:8 title "Russell Stanley" w lines, 'commits_by_author.dat' using 1:9 title "David Sutton" w lines, 'commits_by_author.dat' using 1:10 title "Scott" w lines, 'commits_by_author.dat' using 1:11 title "Saxon" w lines, 'commits_by_author.dat' using 1:12 title "Dan Kortschak" w lines, 'commits_by_author.dat' using 1:13 title "Scott Barnard" w lines, 'commits_by_author.dat' using 1:14 title "Ella Marie Di Stasio" w lines, 'commits_by_author.dat' using 1:15 title "ausocean-david" w lines, 'commits_by_author.dat' using 1:16 title "Ella Pietraroia" w lines, 'commits_by_author.dat' using 1:17 title "AusOcean developer" w lines, 'commits_by_author.dat' using 1:18 title "Ella Di Stasio" w lines
