# Readme

This repository contains software and content for the AusOcean
website.

## Deployment
To deploy you must have npm and gcloud SDK installed, then:

- First build the lit elements using:
    `npm run build`
- Then deploy to app engine using:
    `gcloud app deploy app.yaml --project aus-ocean --version 1`

## Development
- Download a key from gcloud for the service account ([Credentials Docs](https://developers.google.com/workspace/guides/create-credentials#create_credentials_for_a_service_account)).
- Set the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to the path of the credential you just downloaded using: `export GOOGLE_APPLICATION_CREDENTIALS=<PATH_TO_CREDENTIALS>`
- Build the lit elements using: `npm run build`
- Start the server using: `npm run serve`
- View the server at [localhost:8080](localhost:8080)

# License

Copyright (C) 2017-2019 the Australian Ocean Lab (AusOcean)

This is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with NetReceiver in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
